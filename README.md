# jTack DB

A Simple, In-Memory key-value database with support for nested transactions
Credit to Thumbtack for the challenge. More info and description to come.

Java port of Tack DB http://github.com/dguardado/tack

## License

Copyright © Dimas Guardado, Jr. All rights reserved.