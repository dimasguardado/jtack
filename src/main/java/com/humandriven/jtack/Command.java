/*
 * Copyright (c) Dimas Guardado, Jr. All rights reserved.
 */

package com.humandriven.jtack;

import java.util.List;

import static com.google.common.base.Optional.fromNullable;

/**
 * This enumeration represents the set of Operations one can perform on the db. It serves as a mechanism for parsing
 * the line input, and mapping the parsed input to executable code. In a more dynamic language I might use a function
 * map.
 *
 * @author dguardado
 */
enum Command {

    GET(1) {
    	@Override
        protected String applyHelper(Database db, List<String> args) {
            return fromNullable(db.get(args.get(0))).or("NULL");
        }
    },

    NUMEQUALTO(1) {
        @Override
        protected String applyHelper(Database db, List<String> args) {
            return String.valueOf(db.numEqualTo(args.get(0)));
        }
    },

    SET(2) {
        @Override
        protected String applyHelper(Database db, List<String> args) {
            db.set(args.get(0), args.get(1));
            return "";
        }
    },

    UNSET(1) {
        @Override
        protected String applyHelper(Database db, List<String> args) {
            db.unset(args.get(0));
            return "";
        }
    },

    BEGIN(0) {
        @Override
        protected String applyHelper(Database db, List<String> args) {
            db.begin();
            return "";
        }
    },

    COMMIT(0) {
        @Override
        protected String applyHelper(Database db, List<String> args) {
            db.commit();
            return "";
        }
    },

    ROLLBACK(0) {
        @Override
        protected String applyHelper(Database db, List<String> args) {
            if (!db.canRollback()) {
                return "INVALID ROLLBACK";
            }

            db.rollback();
            return "";
        }
    };

    private final int arity;

    private Command(int arity) {
        this.arity = arity;
    }

    public String apply(Database db, List<String> args) {
        try {
            if (args.size() < arity) {
                return "INVALID ARGUMENT";
            }

            return applyHelper(db, args);
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    protected abstract String applyHelper(Database db, List<String> args);

}
