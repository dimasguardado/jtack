/*
 * Copyright (c) Dimas Guardado, Jr. All rights reserved.
 */

package com.humandriven.jtack;

import javax.annotation.concurrent.NotThreadSafe;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import static com.google.common.base.Objects.equal;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * This implementation makes the simplifying assumption that only one thread (the console) will access it at any given
 * time. This means, this implementation cannot be exposed as a network server unless some code is put in place to
 * protect the DBs invariants.
 *
 * In order to be space efficient in a world where we don't have Immutable Hash Tries as our Map implementation, we
 * back our database with a standard mutable map. We preserve nested transaction isolation with a Deque of HashMaps
 * representing the DELTA from the base map since that transaction scope was opened.
 *
 * @author dguardado
 */
@NotThreadSafe
public final class Database {

    private final Deque<Map<String, String>> data = new LinkedList<Map<String, String>>();

    /**
     * Initialize database with the base map. This is our non-transactional view
     */
    private Database() {
        data.add(new HashMap<String, String>());
    }

    /**
     * Checks the inner-most transaction scope for the key.
     * If it exists, return it, otherwise check its parent. Lather, rinse, repeat.
     *
     * @param key a database identifier String
     * @return the value associated with the given key
     */
    public String get(String key) {
        checkArgument(!isNullOrEmpty(key), "Key must not be empty");
        Iterator<Map<String, String>> scopes  = data.descendingIterator();
        while (scopes.hasNext()) {
            Map<String, String> scope = scopes.next();
            if (scope.containsKey(key)) {
                return scope.get(key);
            }
        }

        return null;
    }

    /**
     * Count of entries in the db equal to the given value
     *
     * In order to remain consistent across transaction scopes, this implementation tracks the set of keys mapping to a
     * particular value, retracting it from the set if a later scope will have pulled it from the db by UNSET or by
     * SETting that key to a different value
     *
     * @param value a DB value
     * @return the integer number of entries containing the given value, across transaction scopes
     */
    public int numEqualTo(String value) {
        checkArgument(!isNullOrEmpty(value), "Value must not be empty");
        Set<String> keys = new HashSet<String>();
        for (Map<String, String> scope : data) {
            for (Map.Entry<String, String> entry : scope.entrySet()) {
                String k = entry.getKey();
                String v = entry.getValue();
                if (equal(value, v)) {
                    keys.add(k);
                } else {
                    keys.remove(k);
                }
            }
        }

        return keys.size();
    }

    /**
     * Writes a the given value to the location named by key to the innermost transaction scope.
     *
     * @param key key
     * @param value key
     */
    public void set(String key, String value) {
        checkArgument(!isNullOrEmpty(key), "Key must not be empty");
        checkArgument(!isNullOrEmpty(value), "Value must not be empty");
        data.peekLast().put(key, value);
    }

    /**
     * Retracts the entry named by key from the db in the innermost transaction scope.
     *
     * In order to facilitate later merge operations (for COMMIT or NUMEQUALTO), we do not remove the key from the map,
     * we set its value to null.
     *
      * @param key the key
     */
    public void unset(String key) {
        checkArgument(!isNullOrEmpty(key), "Key must not be empty");
        data.peekLast().put(key, null);
    }

    /**
     * Adds a new delta map to the transaction scope deque
     */
    public void begin() {
        data.add(new HashMap<String, String>());
    }

    /**
     * Starting from the base db, merge the data from the next inner scope and remove that scope. Lather, rinse,
     * repeat until there are no more transaction scopes. The result should be that data from the innermost transactional
     * scope should persist at the end of the operation
     */
    public void commit() {
        Map<String, String> db = data.pop();
        while (data.size() > 0) {
            db.putAll(data.pop());
        }

        data.add(db);
    }

    /**
     * Whether or not there is a transaction to rollback
     * @return true if there is a transactional scope, otherwise false
     */
    public boolean canRollback() {
        return data.size() > 1;
    }

    /**
     * Removes the innermost transactional scope from the scope deque
     */
    public void rollback() {
        checkState(canRollback(), "INVALID ROLLBACK");
        data.removeLast();
    }

    /**
     * Main method for console. Iterates over each line of input, parses the command, and calls the appropriate function.
     *
     * This didn't need a whole class unto itself, so I put it here.
     *
     * @param args ignored
     */
    public static void main(String[] args) {

        Database db = new Database();

        System.out.println();
        System.out.println("Welcome to TackDB. What is your command?");
        System.out.println();

        InputStreamReader converter = new InputStreamReader(System.in);
        BufferedReader in = new BufferedReader(converter);

        try {
            for (String raw = ""; !equal(raw, null) && !equal(raw, "END"); raw = in.readLine()) {
                String line = raw.trim();

                if (line.length() < 1) {
                    continue;
                }

                LinkedList<String> form = new LinkedList<String>(Arrays.asList(line.split("\\s+")));

                try {

                    // The Command enumeration allows us to both do this in one line, and perform the action in
                    // a standard, polymorphic fashion.
                    String result = Command.valueOf(form.remove()).apply(db, form);
                    if (equal(result, "")) {
                        continue;
                    }

                    System.out.println(result);
                } catch (Exception e) {
                    System.out.print(e.getMessage());
                }
            }
        } catch (IOException e) {
            System.out.println("INPUT ERROR");
        }

        System.out.println();
        System.out.println("See you next time!");
        System.out.println();


    }

}
